############################################################
#                  DCD Trajectories                        #
############################################################
#
#------ filename of topology file for trajectories
#------ currently only supports pdb and charmm psf format
fnpsf  __WBOXPDB__

#------ filename of trj files, start & end file index
#------ currently only supports dcd and netcdf format
fntrj  __RUNDIR__/run%03d.nc      __NCNUMBER__ __NCNUMBER__

#------ stride, first-frame, last-frame for each trj file.
#------ e.g. 'stride 2 10 100' means '10 12 14 ... 100'
#------ if first-frame & last-frame are missing, the range
#------ is 1 ~ END
stride  1 __FIRSTFRAMENUMBER__ __LASTFRAMENUMBER__ 

#------ A ref pdb file for alignment. If this entry is
#------ empty, the first frame in fntrj will serve as the
#------ ref structure. Note: this entry is meaningful only
#------ if align section below has been turned on.
fnref  




############################################################
#                    Frame Alignment                       #
############################################################
#
#====== whether to superimpose frames
align  false  #<=== true/false

#------ which segments to be focused. Using $ for blank
#------ empty means all segments
#seg_id  SEGA SEGB
seg_id  $$$$

#------ which resids to search; blank means all resids
resid  1-58

#------ which residue names to search; blank means all names
resname  

#------ atoms to be fitted. Normally 'N CA C' or 'CA'
atname  CA

#------ which file name to save the rmsd data
fnout  rmsd.dat

#------ output as ascii(true) or binary(false)
ascii  false



############################################################
#                     Job Definition                       #
############################################################
#
#
#>>>>>>>>>>>>> Calculating radius of gyration <<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
rgyr  false  #<=== true/falsefalse

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid  

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname  HN  949
atname  heavy

#------ which file name to save the result
fnout  rgyr.tab

#------ output as ascii(true) or binary(false)
ascii  true
#===========================================================


#>>>>>>>>>>>>>>>   Extracting coordinates   <<<<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
coor  true  #<=== true/false

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid 1-15 

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname    HN  949
atname C    CA   CB   CD   CD1  CD2  CE1  CE2  CG   CG1  CG2  CZ   HD11  HD12  HD13  HD21  HD22  HG12  HG13  HG21  HG22  HG23  H    H2   H3   HA   HA2  HA3  HB   HB1  HB2  HB3  HD1  HD2  HD3  HE1  HE2  HG   HG1  HG2  HG3  HH   N    ND2  O    OD1  OE1  OE2  OG1  OH   OXT  SG   

#------ which file name to save the result
fnout  __OUTPUTFILENAME__

#------ output as ascii(true) or binary(false)
ascii  true
#===========================================================


#>>>>>>>>>>>>>>>     Extracting vectors     <<<<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
vect  false  #<=== true/false

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid  

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname  HN  949
atname  N H

#------ which file name to save the result
fnout  vect.dat

#------ output as ascii(true) or binary(false)
ascii  false
#===========================================================


#>>>>>>>>>>>>>>>    Extracting distances    <<<<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
dist  false  #<=== true/false

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid  

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname    HN  949
atname  N HN

#------ which file name to save the result
fnout  dist.tab

#------ output as ascii(true) or binary(false)
ascii  true
#===========================================================


#>>>>>>>>>>>>>>>     Extracting angles      <<<<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
angl  false  #<=== true/false

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid  

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname    HN  949
atname  HN N CA

#------ which file name to save the result
fnout  angl.tab

#------ output as ascii(true) or binary(false)
ascii  true
#===========================================================


#>>>>>>>>>>>>>>> Extracting dihedral angles <<<<<<<<<<<<<<<#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
dihe  false  #<=== true/false

#------ which segments to be analyzed. Using $ for blank
#------ empty means all segments
#seg_id  PROT PEP$ XWAT
seg_id  $$$$

#------ if leave it blank, all resid are analyzed
#resid  1-5 7-21 26 30-76
resid  

#------ if leave it blank, all resname are analyzed
#resname  LEU ILE VAL
resname  

#------ use atom # if you want to fix this atom
#------ e.g. in the case of spin label
#atname  HN  949
#------ use +/- to indicate next/previous residue
#------ e.g. to get backbone phi and psi, use
#atname  -C N CA C    N CA C +N
atname  HN N CA HA

#------ which file name to save the result
fnout  dihe.tab

#------ output as ascii(true) or binary(false)
ascii  true
#===========================================================
