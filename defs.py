#!/usr/bin/python
# -*- coding: utf-8 -*-


from pdblib.base import *
import subprocess
import glob
import re
import shutil
import os
import StringIO




class molecule_specific_parameters:

    def __init__(self):
        self.input_dir = "./"

        self._min1 = "min1"
        self._min2 = "min2"
        self._heat = "heat"
        self._run  = "run"
        self._trjtool_all_atoms = "all_atoms.trjtool.inp"
        self.pdb  = ""

        self.CYXs = None
        self.GLHs = None
        self.wbox_width = 29  # Angstrom
        self.ssbonds = None # example [ (4,7), (12,15) ]

        self.run_start = 1
        self.run_end = 1

        self.frames_per_nc = 1

    @property
    def trjtool_all_atoms (self):
        return self.input_dir+"/"+self._trjtool_all_atoms

    @property
    def min1 (self):
        return self.input_dir+"/"+self._min1
    @property
    def min2 (self):
        return self.input_dir+"/"+self._min2
    @property
    def heat (self):
        return self.input_dir+"/"+self._heat
    @property
    def run  (self):
        return self.input_dir+"/"+self._run



# parameters that common for most of runs
class common_parameters:

    def __init__(self):

        self._converted_pdb= "input.pdb"

        self._tleap_input = "tleap.rc"
        self._wbox = "wbox"

        self.work_dir = "./"

        self._build_dir = "1_build"
        self._minimize_dir = "2_minimize"
        self._heat_dir = "3_heat"
        self._run_dir = "4_run"

        self._min1 = "min1"
        self._min2 = "min2"
        self._heat = "heat"
        self._run = "run"


        self._log = "log.txt"
        self.verbose = 1

        self._amberhome= "/opt/amber14/"
        self._tleap= "tleap"
        self._pmemd= "pmemd"
        self._leaprc = self.amberhome+"dat/leap/cmd/oldff/leaprc.ff99SBildn"
        self.trjtool = "trjtool"

        self._gpu=1



    def check(self):
        if (not os.path.isfile(self.leaprc) ): raise Exception("No such file "+self.leaprc)
        if (not os.path.isfile(self.tleap) ):  raise Exception("No such file "+self.tleap)
        if (not os.path.isfile(self.pmemd) ):  raise Exception("No such file "+self.pmemd)

    def prepare_dirs(self):
        if not os.path.exists(self.work_dir):
            os.makedirs(self.work_dir)

        for sub_dir in [self._build_dir, self._minimize_dir, self._heat_dir, self._run_dir]:
            if not os.path.exists(self.work_dir+"/"+sub_dir):
                os.makedirs(self.work_dir+"/"+sub_dir)


    @property
    def converted_pdb(self):
        return self.work_dir+"/"+self._build_dir+"/"+self._converted_pdb


    @property
    def min1(self):
        return self.work_dir+"/"+self._minimize_dir+"/"+self._min1


    @property
    def min2(self):
        return self.work_dir+"/"+self._minimize_dir+"/"+self._min2




    @property
    def heat(self):
        return self.work_dir+"/"+self._heat_dir+"/"+self._heat


    @property
    def run(self):
        return self.work_dir+"/"+self._run_dir+"/"+self._run


    @property
    def tleap_input(self):
        return self.work_dir+"/"+self._build_dir+"/"+self._tleap_input

    @property
    def leaprc(self):
        return self._leaprc

    @leaprc.setter
    def leaprc(self, value):
        self._leaprc = value



    @property
    def wbox(self):
        return self.work_dir+"/"+self._build_dir+"/"+self._wbox



    @property
    def amberhome(self):
        return self._amberhome

    @amberhome.setter
    def amberhome(self, value):
        self._amberhome = value



    @property
    def tleap(self):
        return self.amberhome+"/bin/"+self._tleap



    @property
    def pmemd(self):
        return self.amberhome+"/bin/"+self._pmemd




    @property
    def gpu(self):
        return self._gpu

    @gpu.setter
    def gpu(self, value):
        self._gpu = value



class runMD:
    m = molecule_specific_parameters()
    c = common_parameters()

    _current_step = None

    @property
    def current_step(self):
        if (self._current_step == None):
            return self.m.run_start-1
        else:
            return self._current_step

    def __init__(self):
        pass

    def build(self):
        self.log("Build parameters...")
        convert(self.m.pdb, self.c.converted_pdb, self.m.CYXs, self.m.GLHs)

        solvateParameter = float(subprocess.check_output( ["getSolvateParam", self.c.converted_pdb, str(self.m.wbox_width)]))

        # write tleap input file
        tleaprc = open(self.c.tleap_input, 'w')

        tleaprc.writelines(open(self.c.leaprc).readlines())

        tleaprc.write("wbox = loadpdb "+self.c.converted_pdb+"\n")

        tleaprc.write("solvateoct wbox SPCBOX " + str(solvateParameter)+"\n")

        # set SS bonds
        if (self.m.ssbonds):
            for b in self.m.ssbonds:
                tleaprc.write("bond wbox."+str(b[0])+".SG wbox."+str(b[1])+".SG")


        tleaprc.write(
        """

        addions wbox Na+ 0
        saveamberparm wbox """+self.c.wbox+""".prmtop """+self.c.wbox+""".inpcrd
        savepdb wbox """+self.c.wbox+""".pdb
        quit

        """)
        tleaprc.close()

        subprocess.check_output([self.c.tleap, "-s", "-f", self.c.tleap_input])

    def minimize(self):
        self.log("Minimize...")
        os.environ["CUDA_VISIBLE_DEVICES"]=str(self.c.gpu) # consider c.gpu is list
        cmd =  [self.c.pmemd,
         "-O",
         "-i",  self.m.min1 + ".in",
         "-o",  self.c.min1 + ".out",
         "-p",  self.c.wbox + ".prmtop",
         "-c",  self.c.wbox + ".inpcrd",
         "-r",  self.c.min1 + ".rst",
         "-ref",self.c.wbox + ".inpcrd",
         "-inf", self.c.min1 + ".mdinfo",
         "-l",   self.c.min1 + ".log"
        ]

        # print " ".join(cmd)

        subprocess.call( cmd )

        subprocess.call(
        [self.c.pmemd,
         "-O",
         "-i", self.m.min2 + ".in",
         "-o", self.c.min2 + ".out",
         "-p", self.c.wbox + ".prmtop",
         "-c", self.c.min1 + ".rst",
         "-r", self.c.min2 + ".rst",
         "-inf", self.c.min2 + ".mdinfo",
         "-l",   self.c.min2 + ".log"
        ])

    def heat(self):
        self.log("Heat...")
        os.environ["CUDA_VISIBLE_DEVICES"]=str(self.c.gpu) # consider c.gpu is list

        subprocess.call(
        [self.c.pmemd,
         "-O",
         "-i",   self.m.heat + ".in",
         "-o",   self.c.heat + ".out",
         "-p",   self.c.wbox + ".prmtop",
         "-c",   self.c.min2 + ".rst",
         "-x",   self.c.heat + ".cdf",
         "-r",   self.c.heat + ".rst",
         "-ref", self.c.min2 + ".rst",
         "-inf", self.c.heat + ".mdinfo",
         "-l",   self.c.heat + ".log"

        ]
        )

        shutil.copyfile(self.c.heat + ".rst",self.c.run + "%03d"%(self.m.run_start-1)+".rst")

    def run(self):

        os.environ["CUDA_VISIBLE_DEVICES"]=str(self.c.gpu) # consider c.gpu is list

        for i in range(self.current_step+1, self.m.run_end+1):
            self.doStep()

    def doStep(self):
        self.log("Running step "+str(self.current_step+1)+" [ of "+str(self.m.run_end)+"] ...")
        os.environ["CUDA_VISIBLE_DEVICES"]=str(self.c.gpu) # consider c.gpu is list
        i = self.current_step+1
        subprocess.call(
                                [self.c.pmemd,
                                 "-O",
                                 "-i", self.m.run + ".in",
                                 "-o", self.c.run + "%03d"%i+".out",
                                 "-p", self.c.wbox+ ".prmtop",
                                 "-c", self.c.run + "%03d"%(i-1)+".rst",
                                 "-x", self.c.run + "%03d"%i+".nc",
                                 "-r", self.c.run + "%03d"%i+".rst",
                                 "-inf", self.c.run + ".mdinfo",
                                 "-l",   self.c.run + ".log"
                                ]
                            )

        self.ncToDat(i)
        self.rmNc(i)
        self._current_step = i

    def rmNc(self,i):
        os.remove(self.c.run+"%03d"%i+".nc")

    def getLastRstNumber(self):

        rsts = sorted(glob.glob(self.c.run + "*.rst"))

        for rst in reversed(rsts):
            if (os.path.getsize(rst)>0):
                m = re.match(".*(\d{3})\.rst", rst)
                return int(re.sub("^0*", "", m.group(1)))
        return 0


    def ncToDat(self,ncnumber):
        first_frame = 1
        last_frame = self.m.frames_per_nc
        output_filename = self.c.run + "%03d"%ncnumber + ".dat"
        inp = open(self.m.trjtool_all_atoms).read()\
            .replace("__RUNDIR__", os.path.abspath(self.c.work_dir+"/"+self.c._run_dir))\
            .replace("__WBOXPDB__", os.path.abspath(self.c.wbox+".pdb"))\
            .replace("__NCNUMBER__", str(ncnumber))\
            .replace("__FIRSTFRAMENUMBER__", str(first_frame))\
            .replace("__LASTFRAMENUMBER__", str(last_frame))\
            .replace("__OUTPUTFILENAME__", str(output_filename))


        inpfilename = "/tmp/"+str(os.getpid())+".inp"

        inpfile = open(inpfilename, 'w')
        inpfile.write(inp)
        inpfile.close()

        devnull= open(os.devnull)

        subprocess.call( [self.c.trjtool,inpfilename], stdout=devnull)

        devnull.close()

        os.remove(inpfilename)

    def getFrame(self, nc, frame):
        if frame >= self.m.frames_per_nc:
            return None

        filename = self.c.run+"%03d"%nc+".dat"
        frame = int(frame)

        file = open (filename)
        lines=file.readlines()
        natoms = int(lines[0])

        pdb_filename = "/tmp/"+str(os.getpid())+".pdb"
        pdb = open(pdb_filename, 'w')

        for i in xrange(0,natoms):
            s=re.split("\s+",lines[i+1].strip())
            resserial =int(s[0])
            resname = s[1]
            aname = s[2]
            if (len(aname)<=3):
                aname = " "+aname
            aserial = i+1
            s=re.split("\s+",lines[i+1+(frame+1)*natoms].strip())
            x,y,z = float(s[0]),float(s[1]),float(s[2])
            print >>pdb, "%6s%5d %4s %3s  %4d    %8.3f%8.3f%8.3f"%("ATOM  ",aserial,aname.ljust(4),resname,resserial,x,y,z)

        print >> pdb

        pdb.close()
        mol = Mol(pdb_filename)
        os.remove(pdb_filename)

        return mol

    def log(self, log_string):

        l = open(self.c.work_dir+"/"+self.c._log,"a")
        l.write(log_string+"\n")
        l.close()

        if self.c.verbose >= 1:
            print log_string







def convert(input_pdb, output_pdb, CYXs=None, GLHs=None):

    if CYXs is None: CYXs = []
    if GLHs is None: GLHs = []

    mol = Mol(input_pdb)

    for res in getreses(mol):
        if res.resi in CYXs:
            if res.name not in ['CYS', 'CYX']:
                print('ERROR: please double check resi of CYS!')
                exit(1)
            res.name = 'CYX'
        elif res.resi in GLHs:
            if res.name not in ['GLU', 'GLH']:
                print('ERROR: please double check resi of GLU!')
                exit(1)
            res.name = 'GLH'

    for at in getats(mol):
        if at.gettype() == 'H':
            at.r = ()

    mol.renumber(1, 1)
    mol.write(output_pdb)