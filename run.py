#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import glob
import defs
import multiprocessing
import multiprocessing.queues
import re
import glob

from pdblib.base import *





# list pdbs to process
#pdb_list = ["prot.pdb"]
pdb_list = glob.glob("./goods/*.pdb")
#pdb_list = [ "TESTPY.pdb"]
#pdb_list = sys.argv[1]

# select gpus that would be used
gpus = [ 0,1,2,3 ]



capture_radius = 3.5

def findCloseAtoms(atoms):
    close = []
    for i in range(len(atoms)):
        for j in range(i+1, len(atoms)):
            a,b = atoms[i],atoms[j]
            dist = atdist(a,b)
            if dist < capture_radius:
                close.append( (a.resi, b.resi) )
    return close

def run_simulation(pdb_filename, gpuid):

    # change default parameters if you need

    MD = defs.runMD()

    # Common parameters at first
    MD.c.work_dir = "./"+os.path.splitext(os.path.basename(pdb_filename))[0]+"_2fs"
    MD.c._pmemd = "pmemd.cuda"
    MD.c.gpu = gpuid
    MD.c.check()
    MD.c.prepare_dirs()

    # Molecule-specific parameters after
    MD.m.input_dir = "./in_files"
    MD.m.pdb = pdb_filename
    MD.m.CYXs = None #[4,7,12,15]
    MD.m.GLHs = None
    MD.m.wbox_width = 25  # Angstrom

    # example: MD.m.ssbonds = [ (4,7), (12,15) ]
    MD.m.ssbonds = findCloseAtoms([ at for at in Mol(MD.m.pdb).getats() if at.name == "SG"])

    MD.m.frames_per_nc = 1000



    MD.m.run_start = min(MD.getLastRstNumber()+1,100)
    MD.m.run_end = 120


    

    if MD.m.run_start == 1:
        MD.build()
        MD.minimize()
        MD.heat()


    for n in xrange(MD.m.run_start, MD.m.run_end+1):
        MD.doStep()
        if (MD.m.ssbonds):
            bonded_sulfurs = [ s for bond in MD.m.ssbonds for s in bond ]
        else:
            bonded_sulfurs = []
            
        if (n>100):
          for iframe in xrange(MD.m.frames_per_nc):
              mol = MD.getFrame(n, iframe)
              sulfur = [ at for at in mol.getats() if at.name=="SG" and at.resi not in bonded_sulfurs ]
              close = findCloseAtoms(sulfur)
              if len(close) > 0:
                  MD.m.ssbonds.append(close[0])
                  MD.log("Step " + str(n) +", frame " + str(iframe) + ": new sulfur bond: " + str(close[0][0])+" <-> "+str(close[0][1]))
                  pdbframename=MD.c.work_dir+"/"+MD.c._build_dir+"/"+"prot%03d:%03d.pdb"%(n,iframe)
                  mol.write(pdbframename)
                  MD.m.pdb = pdbframename
                  MD.build()
                  MD.minimize()
                  MD.heat()
                  break
        if (MD.current_step==100):
            mol = MD.getFrame(n, MD.m.frames_per_nc-1)
            pdbframename=MD.c.work_dir+"/"+MD.c._build_dir+"/"+"prot%03d:%03d.pdb"%(n, MD.m.frames_per_nc-1)
            mol.write(pdbframename)
            MD.m.pdb = pdbframename
            
            MD.build()
            MD.minimize()
            MD.heat()
      






# # # #  Do not edit text below  ! ! !
# # # #  Do not edit text below  ! ! !
# # # #  Do not edit text below  ! ! !



Q = multiprocessing.queues.SimpleQueue()

for pdb in pdb_list:
    Q.put(pdb)


class P(multiprocessing.Process):
    def __init__(self, gpu_id):
        super(P, self).__init__()
        self.gpu = gpu_id

    def run(self):
        while (not Q.empty()):
            pdbfilename=Q.get()
            run_simulation(pdbfilename, self.gpu)
            print pdbfilename,'assigned to', self.gpu, "gpu"




procs = [P(i) for i in gpus]

for p in procs:
    p.start()

for p in procs:
    p.join()
